# Aurelium-Project

CI project to build, test and deploy both the minetest server binaries and the 
modules.

## Building

Merging to the master branch will start a full deployment to the dev server.

TODO: A tag release will deploy to the production server.

Pushing to a branch named mods-<somebranch name> will automatically deploy the 
modules from the modules-list.json file, to the dev server.
This will not trigger a build or a full deployment.

## Modules

Here are examples of two different types of JSON arrays describing modules to 
deploy.

A stand-alone module:
```
  "https://github.com/AiTechEye/smartshop.git":{ "version": "master", "name": "smartshop", "modpack":false, "enabled": true   },
```

A modpack with several sub-modules:
```
  "https://github.com/pyrollo/display_modpack.git":{ "version": "master", "name": "display_modpack", "modpack":true,
    "SubMods":{
      "boards": true,
      "display_api": true,
      "font_api": true,
      "font_metro": true,
      "ontime_clocks": true,
      "signs": true,
      "signs_api": true,
      "signs_road": true,
      "steles": true      
    }
  },
```

## Stand alone module definition:
The key for each module or modpack is the URL of the git repository.

The "version" can be defined, but is probably master in most cases.

The "name" of the module refers mainly to the directory specified as the target 
in the git clone command.

The "enabled" value, refers to the state of the module and will set the module 
flag to true or false in the world.mt file.

## Modpack module definition:
The modpack definition is similar except that it doesn't have an 'enabled' 
property.  Instead each sub-modules are defined as simple key-value pairs within
the SubMods nested array.


