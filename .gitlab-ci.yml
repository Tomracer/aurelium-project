# Copyright (C) 2023 za267@hotmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

image: "fedora:37"

variables:
  #SSH_OPTIONS: -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -l cideploy -q
  SSH_OPTIONS: -o GSSAPIAuthentication=no
  SSH_HOST: $DEV_USER@$DEV_HOST -p $DEV_PORT
  DEV_HOME: /home/$DEV_USER
  MT_BUILD_VER: Aurelium

# Prep the environment to have the necessary tools to build.
before_script:
  - sudo dnf install openssh-clients iputils git -y
  - eval $(ssh-agent -s)
  - ssh-add <(echo "$RUNNER_DEPLOY_KEY" | base64 --decode)
  - mkdir -p ~/.ssh
  - chmod 700 ~/.ssh
  - ssh-keyscan -p $DEV_PORT $DEV_HOST >> ~/.ssh/known_hosts
  - chmod 644 ~/.ssh/known_hosts
  - scp -P $DEV_PORT ./deploy.sh $DEV_USER@$DEV_HOST:~/bin/
  - scp -P $DEV_PORT ./deploy-modules.sh $DEV_USER@$DEV_HOST:~/bin/
  - ssh $SSH_HOST 'chmod +x ~/bin/deploy.sh'
  - ssh $SSH_HOST 'chmod +x ~/bin/deploy-modules.sh'

stages:
  - sshtest
  - debug
  - build
  - pull_mods
  - deploy

# SSH debugging code.  Will only run on a debug-* branch.
sshing:
  stage: sshtest
  script:
    - ssh $SSH_HOST 'uname -a'
  only:
    refs:
      - /^debug-.*$/

# Special debugging code.  Will only run on a debug-* branch.
debugging:
  stage: debug
  script:
    - ls -la
    - chmod +x clone-modules.sh
    - sh clone-modules.sh modules-list.json
  only:
    refs:
      - /^debug-.*$/

build-minetest:
  stage: build
  script:
    - sudo dnf install podman -y
    - podman login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - podman build -t "$CI_REGISTRY"/aurelium/aurelium-project/minetest:latest .
    - podman push "$CI_REGISTRY"/aurelium/aurelium-project/minetest:latest
    # - ssh $SSH_HOST 'uname -a'
    # - ssh $SSH_HOST 'sudo dnf install make automake gcc gcc-c++ kernel-devel cmake libcurl-devel openal-soft-devel 
    #  libvorbis-devel libXxf86vm-devel libogg-devel freetype-devel mesa-libGL-devel zlib-devel 
    #  jsoncpp-devel irrlicht-devel bzip2-libs gmp-devel sqlite-devel luajit-devel leveldb-devel 
    #  ncurses-devel doxygen spatialindex-devel bzip2-devel postgresql-server postgresql-server-devel postgresql 
    #  libpq-devel -y'
    # - ssh $SSH_HOST 'rm -Rf ./build'
    # - ssh $SSH_HOST 'mkdir -p build'
    # - ssh $SSH_HOST 'cd build && git clone --depth 1 --branch stable-5 https://github.com/minetest/minetest.git'
    # - ssh $SSH_HOST 'cd build/minetest && 
    #  git clone --depth 1 --branch stable-5 https://github.com/minetest/minetest_game.git games/minetest_game && 
    #  cmake . -DRUN_IN_PLACE=FALSE -DBUILD_SERVER=true -DBUILD_CLIENT=FALSE -DCMAKE_BUILD_TYPE=Release -DENABLE_CURL=TRUE 
    #  -DENABLE_CURSES=TRUE -DENABLE_FREETYPE=TRUE -DENABLE_GETTEXT=TRUE -DENABLE_GLES=TRUE 
    #  -DENABLE_POSTGRESQL=TRUE -DENABLE_LEVELDB=TRUE -DENABLE_SPATIAL=TRUE -DENABLE_LUAJIT=TRUE 
    #  -DENABLE_SYSTEM_GMP=TRUE -DENABLE_SYSTEM_JSONCPP=TRUE -DVERSION_EXTRA=$MT_BUILD_VER 
    #  -DLUA_INCLUDE_DIR=/usr/include/luajit-2.1/ -DLUA_LIBRARY=/usr/lib64/libluajit-5.1.so && 
    #  make && 
    #  sudo make install'
    # - cd ..
    # - tar -czvf ./minetest.tar.gz minetest/
  only:
    refs:
      - master
  # except:
  #    - pushes
  # artifacts:
  #   paths:
  #   - /builds/aurelium/aurelium-project/build/minetest.tar.gz
  #   expire_in: 1 day

clone_mods_dev:
  stage: pull_mods
  script:
    - yum install jq -y
    - sh clone-modules.sh modules-list.json
  only:
    refs:
      - master
      - /mods-/
  except:
    - pushes

# deploy_staging:
#   stage: deploy
#   script:
#     - cd build/
#     - ls -la
#     - pwd
#     - scp -P $DEV_PORT /builds/aurelium/aurelium-project/build/minetest.tar.gz $DEV_USER@$DEV_HOST:minetest.tar.gz
#     - ssh -t $SSH_HOST 'tar -xzvf minetest.tar.gz'
#     - ssh -t $SSH_HOST 'sudo systemctl stop minetest'
#     - ssh -t $SSH_HOST 'sudo ./bin/deploy.sh /home/'"'$DEV_USER'"'/minetest'
#     - ssh -t $SSH_HOST 'sudo systemctl start minetest'
#   dependencies:
#     - build-minetest
#   only:
#     refs:
#       - master
#   # except:
#   #   #- tags
#   #   - pushes

deploy_modules:
  stage: deploy
  script:
    - ssh -t $SSH_HOST 'sudo systemctl stop minetest-dev'
    - ssh -t $SSH_HOST 'sudo /home/cideploy/bin/deploy-modules.sh /home/'"'$DEV_USER'"'/minetest'
    #- /home/cideploy/bin/deploy-modules.sh /home/$DEV_USER/minetest
    - ssh -t $SSH_HOST 'sudo systemctl start minetest-dev'
    #- ssh -t $SSH_HOST 'patch /home/someguy/.minetest/mods/patches/unifieddyes.init.lua.patch /home/someguy/.minetest/mods/unifieddyes/init.lua'
  only:
    refs:
      - master
      - /mods-/
  # except:
  #    - pushes
